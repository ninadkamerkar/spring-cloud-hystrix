package com.ninad.springcloud;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@Value("${eureka.instance.instance-id}")
	private String instanceId;

	@RequestMapping("/hello/{name}")
	public String greeting(@PathVariable("name") String name) {
		System.out.println("Inside the service of instance id = " + instanceId);
		StringBuilder message = new StringBuilder();
		message.append("HELLO " + name.toUpperCase() + ". THIS MESSAGE IS RETURNED FROM SERVICE RUNNING ON INSTANCE "
				+ instanceId);
		return message.toString();
	}
}