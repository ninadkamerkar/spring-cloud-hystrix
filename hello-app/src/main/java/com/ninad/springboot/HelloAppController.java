package com.ninad.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

@RestController
public class HelloAppController {
	//THIS IS JUST GETTING 1 WORKING PORT
	private int port;
	
	@Autowired
	private EurekaClient eurekaClient;
	//END
	
	@Value("${hello.service.name}")
	private String helloServiceName;

	@Autowired
	private RestTemplate loadBalancedRestTemplate;

	@Bean
	@LoadBalanced
	public RestTemplate loadBalancedRestTemplate() {
		return new RestTemplate();
	}

	@Autowired
	private RestTemplate normalRestTemplate;

	@Bean
	public RestTemplate normalRestTemplate() {
		return new RestTemplate();
	}

	@RequestMapping("/greeting/loadBalanced/{name}")
	public String greetingLoadBalanced(@PathVariable("name") String name) {
		return loadBalancedRestTemplate.getForObject("http://" + helloServiceName + "/hello/" + name, String.class);
	}

	@RequestMapping("/greeting/normal/{name}")
	public String greetingNormal(@PathVariable("name") String name) {

		// THIS CODE IS TO JUST GET THE PORT OF ONE OF THE RUNNING HELLO SERVICE
		// AS THE PORTS ARE DYNAMIC ALOTTED
		if (port == 0) {
			InstanceInfo instanceInfo = eurekaClient.getNextServerFromEureka(helloServiceName, false);
			port = instanceInfo.getPort();
		}
		// END

		return normalRestTemplate.getForObject("http://localhost:" + port + "/hello/" + name, String.class);
	}
}